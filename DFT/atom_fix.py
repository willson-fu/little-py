def file_to_list(filename):
    file = open(filename, 'r')
    flist = file.readlines()
    reslist = [ele.strip('\n') for ele in flist]
    file.close()
    return reslist


def key_line(flist):
    for i in range(len(flist)):
        if flist[i] == 'Direct':
            return i
    print("There's not 'Direct' in the file, please check again.")
    return 0


def sinappend(flist, atomnum, app_text):
    n = key_line(flist)
    flist[atomnum + n] = flist[atomnum + n] + app_text


def manyappend(flist, atomnumlist, app_text):
    n = key_line(flist)
    for atomnum in atomnumlist:
        flist[atomnum + n] = flist[atomnum + n] + app_text


def restappend(flist, app_text):
    n = key_line(flist)
    for i in range(n + 1, len(flist)):
        if flist[i][-1] == ' ':
            flist[i] = flist[i] + app_text


def list_to_file(flist, filename):
    file = open(filename, 'w')
    for tex in flist:
        file.write(tex)
        file.write('\n')
    file.close()


if __name__ == '__main__':
    mylist = file_to_list('POSCAR')
    numlist = [16, 24, 25, 33, 43, 46, 52, 58, 68, 76, 80, 82]
    manyappend(mylist, numlist, ' F F F')
    sinappend(mylist, 14, ' T T F')
    restappend(mylist, ' T T T')
    list_to_file(mylist, 'POSCAR')
